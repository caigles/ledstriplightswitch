#ifndef _MACHINE_HPP
#define _MACHINE_HPP

#include "Arduino.h"
#include "Buzzer.hpp"
#include "Led.hpp"
#include "SoftTimer.hpp"
#include "Sensor.hpp"

// I/O pins
const int DebugLedOut = 13;
const int LedPwmOut = 7;
const int ProxSwitchIn = 6;
const int BuzzOut = 8;
// PIR sensor polarity
const bool ActiveHigh = false;
// Time intervals (ms)
const long TRampUP = 2000L;
const long TRampDown = 5000L;
const long TBuzzOn = 75L;
const long TBuzzOff = 925L;
const long TWaitDelay = 10000L;
// PWM limit values
const byte MinVal = 5;
const byte MaxVal = 255;

class Machine {
	// Nested abstract class State
	class State {
		protected:
			Machine * _sm;
		public:
			virtual void run() = 0;
			void setContext(Machine * context);
	};

	// Concrete states
	class StIdle : public State {
		public:
			void run();
	};
	class StLightingUp : public State {
		public:
			void run();
	};
	class StConfirmOn : public State {
		public:
			void run();
	};
	class StLightOn : public State {
		public:
			void run();
	};
	class StOffDelay : public State {
		public:
			void run();
	};
	class StLightingDown : public State {
		public:
			void run();
	};
	class StConfirmOff : public State {
		public:
			void run();
	};
	class StWaitIdle : public State {
		public:
			void run();
	};

	// Machine class members
	private:
		State * state;
		Buzzer buzz = Buzzer(BuzzOut, TBuzzOn, TBuzzOff);
		Led led = Led(LedPwmOut, MinVal, MaxVal, TRampUP, TRampDown);
		Sensor prox = Sensor(ProxSwitchIn, DebugLedOut, ActiveHigh);
		SoftTimer tmr = SoftTimer(TWaitDelay);
	public:
		StIdle stIdle;
		StLightingUp stLightingUp;
		StConfirmOn stConfirmOn;
		StLightOn stLightOn;
		StOffDelay stOffDelay;
		StLightingDown stLightingDown;
		StConfirmOff stConfirmOff;
		StWaitIdle stWaitIdle;

		Machine();
		void update();
};

#endif