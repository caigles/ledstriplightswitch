#ifndef _BUZZER_HPP
#define _BUZZER_HPP

#include "Arduino.h"

class Buzzer {
  private:
    int _pinId;
    long _tOn;
    long _tOff;
    long _tLastChange;
    int _count;
  public:
    Buzzer(int pinId, long tOn, long tOff);
    Buzzer(int pinId);
    void init(int pinId, long tOn, long tOff);
    void init(int pinId, long tOnOff);
    void init(int pinId);
    void beep(int cycles);
    void beep();
    void beepContinuous();
    void beepStop();
    void update();
};

#endif