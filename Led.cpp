#include "Led.hpp"

#define LED_DEBUG

Led::Led(int pinId, int minValue, int maxValue, long tUp, long tDown) {
	init(pinId, minValue, maxValue, tUp, tDown);
}

Led::Led(int pinId, long tUp, long tDown) {
	init(pinId, tUp, tDown);
}

Led::Led(int pinId) {
	init(pinId);
}

void Led::init(int pinId, int minValue, int maxValue, long tUp, long tDown) {
	_pinId = pinId;
	_tUp = tUp;
	_tDown = tDown;
	_minValue = minValue;
	_maxValue = maxValue;
	_value = 0;
	_tLastUpdate = 0;
	_state = L_OFF;
	pinMode(_pinId, OUTPUT);
}

void Led::init(int pinId, long tUp, long tDown) {
	init(pinId, 0, 255, tUp, tDown);
}

void Led::init(int pinId) {
	init(pinId, 0, 255, 10000L, 10000L);
}

void Led::update() {
	byte value = _value;
	long dVal = 0;
	switch(_state) {
		case L_OFF:
			value = 0;
			break;
		case L_ON:
			value = 255;
			break;
		case L_MIN:
			value = _minValue;
			break;
		case L_MAX:
			value = _maxValue;
			break;
		case L_RAMPING_DOWN:
			dVal = decreaseValue();
			if (_value + dVal <= _minValue) {
				#ifdef LED_DEBUG
				Serial.println(F("	Led->update(): L_RAMPING_DOWN to L_MIN"));
				#endif
				_state = L_MIN;
			}
			break;
		case L_RAMPING_UP:
			dVal = increaseValue();
			if (_value + dVal >= _maxValue) {
				#ifdef LED_DEBUG
				Serial.println(F("	Led->update(): L_RAMPING_UP to L_MAX"));
				#endif
				_state = L_MAX;
			}
			break;
		default:
			break;
	}
	setValue((byte)(value + dVal));
}

void Led::turnOn() {
	switch(_state) {
		case L_OFF:
		case L_MIN:
		case L_MAX:
			#ifdef LED_DEBUG
			Serial.println(F("	Led->turnOn(): Changing to L_ON"));
			#endif
			_state = L_ON;
			break;
		default:
			#ifdef LED_DEBUG
			Serial.println(F("	Led->turnOn(): NO CHANGE"));
			#endif
			break;
	}
	update();
}

void Led::turnOff() {
	#ifdef LED_DEBUG
	Serial.println(F("	Led->turnOff(): Changing to L_OFF"));
	#endif
	_state = L_OFF;
	update();
}

void Led::rampUp() {
	switch(_state) {
		case L_OFF:
		case L_RAMPING_DOWN:
		case L_MIN:
			#ifdef LED_DEBUG
			Serial.println(F("	Led->rampUp(): Changing to L_RAMPING_UP"));
			#endif
			_tLastUpdate = millis();
			_state = L_RAMPING_UP;
			break;
		//case L_ON:
		//case L_RAMPING_UP:
		//case L_MAX:
		default:
			#ifdef LED_DEBUG
			Serial.println(F("	Led->rampUp(): NO CHANGE"));
			#endif
			break;
	}
	update();
}

void Led::rampDown() {
	switch(_state) {
		case L_OFF:
		case L_RAMPING_UP:
		case L_MAX:
			#ifdef LED_DEBUG
			Serial.println(F("	Led->rampDown(): Changing to L_RAMPING_DOWN"));
			#endif
			_tLastUpdate = millis();
			_state = L_RAMPING_DOWN;
			break;
		//case L_ON:
		//case L_RAMPING_DOWN:
		//case L_MIN:
		default:
			#ifdef LED_DEBUG
			Serial.println(F("	Led->rampDown(): NO CHANGE"));
			#endif
			break;
	}
	update();
}

void Led::setValue(byte value) {
	_value = value;
	analogWrite(_pinId, _value);
}

byte Led::getValue() {
	return _value;
}

void Led::setMinValue(byte minValue) {
	_minValue = minValue;
}

byte Led::getMinValue() {
	return _minValue;
}

void Led::setMaxValue(byte maxValue) {
	_maxValue = maxValue;
}

byte Led::getMaxValue() {
	return _maxValue;
}

long Led::increaseValue() {
	long dVal = 0;
	if (_value < _minValue) {
		dVal = _minValue - _value;
	}
	else if (_value >= _maxValue) {
		dVal = _maxValue - _value;
	}
	else if (_value < _maxValue) {
		long now = millis();
		dVal = (now - _tLastUpdate) * (_maxValue - _minValue);
		dVal = dVal / _tUp;
		if (dVal >= 1) {
			dVal = (dVal > (_maxValue - _value)) ? (_maxValue - _value) : dVal;
			_tLastUpdate = now;
		}
	}
	return dVal;
}

long Led::decreaseValue() {
	long dVal = 0;
	if (_value > _maxValue) {
		dVal = _maxValue - _value;
	}
	else if (_value <= _minValue) {
		dVal = _minValue - _value;
	}
	else if (_value > _minValue) {
		long now = millis();
		dVal = (now - _tLastUpdate) * (_maxValue - _minValue);
		dVal = dVal / _tDown;
		if (dVal >= 1) {
			dVal = (dVal > (_value - _minValue)) ? (_value - _minValue) : dVal;
			_tLastUpdate = now;
		}
	}
	return -dVal;
}

bool Led::isState(LedState state) {
	return _state == state;
}

bool Led::isOff() { return isState(L_OFF); }

bool Led::isOn() { return isState(L_ON); }

bool Led::isMin() { return isState(L_MIN); }

bool Led::isMax() { return isState(L_MAX); }

bool Led::isRampingUp() { return isState(L_RAMPING_UP); }

bool Led::isRampingDown() { return isState(L_RAMPING_DOWN); }