#include "Machine.hpp"

#define ST_DEBUG

void Machine::State::setContext(Machine * context) {
	_sm = context;
}

void Machine::StIdle::run() {
	if (_sm->prox.isActive()) {
		#ifdef ST_DEBUG
		Serial.println(F("Idle -> LightingUp"));
		#endif
		//_sm->led.resetRampTimer();
		_sm->led.rampUp();
		_sm->state = &_sm->stLightingUp;
	}
}

void Machine::StLightingUp::run() {
	if (!_sm->prox.isActive()) {
		#ifdef ST_DEBUG
		Serial.println(F("LightingUp -> Idle"));
		#endif
		_sm->led.turnOff();
		_sm->state = &_sm->stIdle;
	}
	else if (_sm->led.isMax()) {
		#ifdef ST_DEBUG
		Serial.println(F("LightingUp -> ConfirmOn"));
		#endif
		_sm->state = &_sm->stConfirmOn;
	}
}

void Machine::StConfirmOn::run() {
	#ifdef ST_DEBUG
	Serial.println(F("ConfirmOn -> LightON"));
	#endif
	_sm->buzz.beep(2);
	_sm->state = &_sm->stLightOn;
}

void Machine::StLightOn::run() {
	if (!_sm->prox.isActive()) {
		#ifdef ST_DEBUG
		Serial.println(F("LightON -> OFFDelay"));
		#endif
		_sm->tmr.start();
		_sm->state = &_sm->stOffDelay;
	}
}

void Machine::StOffDelay::run() {
	if (_sm->prox.isActive()) {
		#ifdef ST_DEBUG
		Serial.println(F("OFFDelay -> LightON"));
		#endif
		_sm->tmr.reset();
		_sm->state = &_sm->stLightOn;
	}
	else if (_sm->tmr.isElapsed()) {
		#ifdef ST_DEBUG
		Serial.println(F("OFFDelay -> LightingDown"));
		#endif
		_sm->tmr.reset();
		_sm->buzz.beep(1);
		_sm->led.rampDown();
		_sm->state = &_sm->stLightingDown;
	}
}

void Machine::StLightingDown::run() {
	if (_sm->prox.isActive()) {
		#ifdef ST_DEBUG
		Serial.println(F("LightingDown -> LightingUp"));
		#endif
		_sm->led.rampUp();
		_sm->state = &_sm->stLightingUp;
	}
	else if (_sm->led.isMin()) {
		#ifdef ST_DEBUG
		Serial.println(F("LightingDown -> ConfirmOFF"));
		#endif
		_sm->state = &_sm->stConfirmOff;
	}
}

void Machine::StConfirmOff::run() {
	#ifdef ST_DEBUG
	Serial.println(F("ConfirmOFF -> WaitIdle"));
	#endif
	_sm->buzz.beep(5);
	_sm->tmr.start();
	_sm->state = &_sm->stWaitIdle;
}

void Machine::StWaitIdle::run() {
	if (_sm->tmr.isElapsed()) {
		#ifdef ST_DEBUG
		Serial.println(F("WaitIdle -> Idle"));
		#endif
		_sm->led.turnOff();
		_sm->state = &_sm->stIdle;
	}
}