#include "Machine.hpp"

Machine::Machine() {
	stIdle.setContext(this);
	stLightingUp.setContext(this);
	stConfirmOn.setContext(this);
	stLightOn.setContext(this);
	stOffDelay.setContext(this);
	stLightingDown.setContext(this);
	stConfirmOff.setContext(this);
	stWaitIdle.setContext(this);

	state = &stIdle;
}

void Machine::update() {
	tmr.update();
	buzz.update();
	prox.update();
	led.update();
	state->run();
}