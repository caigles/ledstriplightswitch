#include "Buzzer.hpp"

void Buzzer::init(int pinId, long tOn, long tOff) {
	_pinId = pinId;
	_tOn = tOn;
	_tOff = tOff;
	_tLastChange = 0;
	_count = 0;
	pinMode(_pinId, OUTPUT);
}

void Buzzer::init(int pinId, long tOnOff) {
	init(pinId, tOnOff, tOnOff);
}

void Buzzer::init(int pinId) {
	init(pinId, 100, 1000);
}

Buzzer::Buzzer(int pinId, long tOn, long tOff) {
	init(pinId, tOn, tOff);
}

Buzzer::Buzzer(int pinId) {
	init(pinId, 100, 1000);
}

void Buzzer::beep(int cycles) {
	if (cycles != 0) {
		_count = cycles;
		digitalWrite(_pinId, HIGH);
		_tLastChange = millis();
	}
}

void Buzzer::beep() {
	beep(1);
}

void Buzzer::beepContinuous() {
	beep(-1);
}

void Buzzer::beepStop() {
	_count = 0;
	digitalWrite(_pinId, LOW);
}

void Buzzer::update() {
	if (_count != 0) {
		long nowMillis = millis();
		long tElapsed = nowMillis - _tLastChange;
		if (digitalRead(_pinId) == HIGH && tElapsed >= _tOn) {
			digitalWrite(_pinId, LOW);
			_tLastChange = nowMillis;
			if (_count > 0) {
				_count--;
			}
		}
		else if (digitalRead(_pinId) == LOW && tElapsed >= _tOff) {
			digitalWrite(_pinId, HIGH);
			_tLastChange = nowMillis;
		}
	}
}
