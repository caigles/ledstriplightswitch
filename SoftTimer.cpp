#include "SoftTimer.hpp"

SoftTimer::SoftTimer(unsigned long tTimeOut) {
	init(tTimeOut);
}

void SoftTimer::init(unsigned long tTimeOut) {
	_tStart = 0;
	_tAcc = 0;
	_state = RESET;
	setTimeOut(tTimeOut);
}
void SoftTimer::update() {
	if (_state == RUNNING) {
		unsigned long now = millis();
		if (_tTimeOut <= _tAcc + now - _tStart) {
			_state = ELAPSED;
		}
	}
}

void SoftTimer::setTimeOut(unsigned long tTimeOut) {
	_tTimeOut = tTimeOut;
}

void SoftTimer::start() {
	update();
	switch (_state) {
		case ELAPSED:
		case RESET:
			_tStart = millis();
			_tAcc = 0;
			_state = RUNNING;
			break;
		case PAUSED:
			_tStart = millis();
			_state = RUNNING;
			break;
		case RUNNING:
		default: ;
	}
}

void SoftTimer::reset() {
	update();
	switch (_state) {
		case RUNNING:
		case PAUSED:
		case ELAPSED:
			_state = RESET;
			break;
		//case RESET:
		default: ;
	}
}

void SoftTimer::pause() {
	update();
	unsigned long now = millis();
	switch (_state) {
		case RUNNING:
			_tAcc += (now - _tStart);
			_state = PAUSED;
			break;
		//case ELAPSED:
		//case PAUSED:
		//case RESET:
		default: ;
	}
}

bool SoftTimer::isState(TimerState state) {
	return _state == state;
}

bool SoftTimer::isReset() { return isState(RESET); }

bool SoftTimer::isRunning() { return isState(RUNNING); }

bool SoftTimer::isPaused() { return isState(PAUSED); }

bool SoftTimer::isElapsed() { return isState(ELAPSED); }
