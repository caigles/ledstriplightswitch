#ifndef _LED_HPP
#define _LED_HPP

#include "Arduino.h"

class Led {
  private:
    enum LedState {L_OFF, L_ON, L_MIN, L_MAX, L_RAMPING_UP, L_RAMPING_DOWN};
    int _pinId;
    byte _value;
    int _minValue;
    int _maxValue;
    long _tUp;
    long _tDown;
    long _tLastUpdate;
    LedState _state;
    bool isState(LedState state);
    long increaseValue();
    long decreaseValue();
    void setValue(byte value);
    byte getValue();
    void setMinValue(byte minValue);
    byte getMinValue();
    void setMaxValue(byte maxValue);
    byte getMaxValue();
  public:
    Led(int pinId, int minValue, int maxValue, long tUp, long tDown);
    Led(int pinId, long tUp, long tDown);
    Led(int pinId);
    void init(int pinId, int minValue, int maxValue, long tUp, long tDown);
    void init(int pinId, long tUp, long tDown);
    void init(int pinId);
    void update();
    void turnOn();
    void turnOff();
    void rampUp();
    void rampDown();
    bool isOff();
    bool isOn();
    bool isMin();
    bool isMax();
    bool isRampingUp();
    bool isRampingDown();
};

#endif