#ifndef _SENSOR_HPP
#define _SENSOR_HPP

#include "Arduino.h"

class Sensor {
  private:
    int _pinId;
    int _testPinId;
    bool _polarity;
    bool _active;	
  public:
    Sensor(int pinId, int testPinId, bool polarity);
    Sensor(int pinId, int testPinId);
    Sensor(int pinId, bool polarity);
    Sensor(int pinId);
    void init(int pinId, int testPinId, bool polarity);
    void init(int pinId, int testPinId);
    void init(int pinId, bool polarity);
    void init(int pinId);
    void update();
    bool isActive();
};

#endif