#include "Sensor.hpp"

void Sensor::init(int pinId, int testPinId, bool polarity) {
	_pinId = pinId;
	_testPinId = testPinId;
	_polarity = polarity;
	pinMode(_pinId, INPUT_PULLUP);
	if (_testPinId > 0)
		pinMode(_testPinId, OUTPUT);
	update();
}

void Sensor::init(int pinId, int testPinId) {
	init(pinId, testPinId, true);
}

void Sensor::init(int pinId, bool polarity) {
	init(pinId, -1, polarity);
}

void Sensor::init(int pinId) {
	init(pinId, -1, true);
}

Sensor::Sensor(int pinId, int testPinId, bool polarity) {
    init(pinId, testPinId, polarity);
}

Sensor::Sensor(int pinId, int testPinId) {
    init(pinId, testPinId);
}

Sensor::Sensor(int pinId, bool polarity) {
    init(pinId, polarity);
}

Sensor::Sensor(int pinId) {
    init(pinId);
}

void Sensor::update() {
	int level = digitalRead(_pinId);
	_active = (_polarity ? level == HIGH : level == LOW);
	if (_testPinId > 0) {
		digitalWrite(_testPinId, _active);
	}
}

bool Sensor::isActive() {
	return _active;
}
