#ifndef _SOFTTIMER_HPP
#define _SOFTTIMER_HPP

#include "Arduino.h"

class SoftTimer {
	private:
		enum TimerState {RESET, RUNNING, PAUSED, ELAPSED};
		unsigned long _tTimeOut;
		unsigned long _tStart;
		unsigned long _tAcc;
		TimerState _state;
		bool isState(TimerState state);
	public:
		SoftTimer(unsigned long tTimeOut);
		void init(unsigned long tTimeOut);
		void update();
		void start();
		void reset();
		void pause();
		bool isReset();
		bool isRunning();
		bool isPaused();
		bool isElapsed();
		void setTimeOut(unsigned long tTimeOut);
};

#endif
